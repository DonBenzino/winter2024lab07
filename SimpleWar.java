import java.util.Scanner;
public class SimpleWar{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Deck deck = new Deck();
		deck.shuffle();
		
		//Player points
		int player1 = 0;
		int player2 = 0;
		
		System.out.println("-------------------------------------------------");
		//Main loop function: Draw Cards --> Print values and current Score --> compare values and update Score accordingly 
		//--> Print new Score --> await enter key press before running again
		while(deck.length() >1){
			Card card1 = deck.drawTopCard();
			Card card2 = deck.drawTopCard();
			System.out.println(card1 + " " +card1.calculateScore() + " vs " + card2 + " " +card2.calculateScore());
			System.out.println("Score: "+ player1 + " to " + player2 +"\n");
			if(card1.calculateScore() == card2.calculateScore()){
				System.out.println("Tie");
			}else if(card1.calculateScore() > card2.calculateScore()){
				player1++;
				System.out.println("Player 1 wins");
			}else{
				player2++;
				System.out.println("Player 2 wins");
			}
			System.out.println("Score: "+ player1 + " to " + player2 +"\n" +"-------------------------------------------------" +"\n"+ "Press enter to continue");
			reader.nextLine();
		}
		
		//Final Score comparing
		if(player1 == player2){
			System.out.println("Tie game");
		}else if(player1 > player2){
			System.out.println("Player 1 won the game");
		}else{
			System.out.println("Player 2 won the game");
		}
	}
}