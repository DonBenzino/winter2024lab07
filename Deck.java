import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numOfCards;
	private Random rng;
	public Deck(){
		this.numOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		for(int i = 0; i<cards.length; i++){
			if(i<13){
				this.cards[i] = new Card("club", i+1);
			}else if(i<26){
				this.cards[i] = new Card("spade", (i+1)-13);
			}else if(i<39){
				this.cards[i] = new Card("heart", (i+1)-26);
			}else{
				this.cards[i] = new Card("diamond", (i+1)-39);
			}
		}
	}
	public int length(){
		return this.numOfCards;
	}
	public Card drawTopCard(){
		this.numOfCards--;
		return this.cards[this.numOfCards];
	}
	public String toString(){
		String result = "";
		for(int i = 0; i<numOfCards;i++){
			result += cards[i].toString() + "\n";
		}
		return result;
	}
	public void shuffle(){
		for(int i = 0; i<numOfCards;i++){
			int swap = this.rng.nextInt(numOfCards-1);
			Card temp = cards[swap];
			this.cards[swap] = this.cards[i];
			this.cards[i] = temp;
		}
	}
}
		