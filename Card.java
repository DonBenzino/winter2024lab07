public class Card{
	private String suit;
	private int val;
	//private boolean used;
	
	public Card(String suit, int val){
		this.suit = suit;
		this.val = val;
		//this.used = false;
	}
	public String getSuit(){
		return this.suit;
	}
	public int getVal(){
		return this.val;
	}
	/*
	public boolean getStatus(){
		return this.used;
	}
	
	public void setStatus(boolean change){
		this.used = change;
	}*/
	public double calculateScore(){
		double result = this.val;
		
		switch(this.suit){
			case "heart":
				result += 0.4;
				break;
			case "spades":
				result += 0.3;
				break;
			case "diamonds":
				result += 0.2;
				break;
			case "clubs":
				result += 0.1;
				break;
			default:
				result = result;
		}
		return result;
	}
	
	public String toString(){
		String faces = "";
		switch(this.val){
			case 1:
				faces = "ACE";
				break;
			case 11:
				faces = "JACK";
				break;
			case 12:
				faces = "QUEEN";
				break;
			case 13:
				faces = "KING";
				break;
			default:
				faces = String.valueOf(this.val);
		}
		return faces + " of " + this.suit;
	}
}